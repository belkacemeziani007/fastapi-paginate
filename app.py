from typing import Any, Iterator

from fastapi import FastAPI, Depends
import uvicorn
from pydantic import BaseModel
from sqlalchemy import column, Integer, String, create_engine
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.ext.declarative import declarative_base

import uvicorn
from fastapi import Depends, FastAPI
from pydantic import BaseModel
from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker

from fastapi_pagination import Page, PaginationParams
from fastapi_pagination.ext.sqlalchemy import paginate

SQLALCHEMY_DATABASE_URL = "mysql+pymysql://root:1234@localhost:3306/housing_prices"
engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)
SessionLocal = sessionmaker(autocommit=True, autoflush=True, bind=engine)

Base = declarative_base(bind=engine)


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(20), nullable=False)
    email = Column(String(20), nullable=False)


Base.metadata.drop_all()
Base.metadata.create_all()


class UserIn(BaseModel):
    name: str
    email: str


class UserOut(UserIn):
    id: int

    class Config:
        orm_mode = True


app = FastAPI()


def get_db() -> Iterator[Session]:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/users", response_model=UserOut)
def create_user(user_in: UserIn, db: Session = Depends(get_db)) -> User:
    user = User(name=user_in.name, email=user_in.email)
    db.add(user)
    db.flush()

    return user


@app.get("/users", response_model=Page[UserOut])
def get_users(db: Session = Depends(get_db), params: PaginationParams = Depends()) -> Any:
    return paginate(db.query(User), params)


if __name__ == "__main__":
    uvicorn.run("app:app")



